<?php

/**
 *	@module			addon_info
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2010-2024 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module 
 *	@platform		see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


$module_directory	= 'addon_info';
$module_name		= 'Addon Info';
$module_function	= 'tool';
$module_version		= '1.2.1';
$module_platform	= '7.x';
$module_delete		=  true;
$module_author		= 'CMS-LAB';
$module_license		= '<a href="https://cms-lab.com/_documentation/addon_info/license.php" target="_blank">GNU General Public License</a>';
$module_license_terms	= '<a href="https://cms-lab.com/_documentation/addon_info/license.php" target="_blank">License terms</a>';
$module_description	= 'Get all addons listed on LEPAdoR.';
$module_guid		= 'ec2a7bfc-610d-4f82-b31b-41fafa7ce8ff';

