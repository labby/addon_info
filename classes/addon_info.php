<?php

declare(strict_types=1);

/**
 *	@module			addon_info
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2010-2024 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 */

class addon_info extends LEPTON_abstract {

	public $database = 0;
	public $admin = 0;
	public $action_url = ADMIN_URL . '/admintools/tool.php?tool=addon_info';

	public static $instance;

	public function initialize()
	{
		$this->database = LEPTON_database::getInstance();
		$this->init_tool();
	}

	public function init_tool( $sToolname = '' )
	{

	}

	public function display($id)
	{
		unset($_SESSION['addon_info_hash']);
		unset($_POST['hash']);

		switch ( $id )
		{
			case "last":
				$content = file_get_contents('https://lepton-cms.com/lepador_last_content.txt');
				break;
			default:
				$content = file_get_contents('https://lepton-cms.com/lepador_alpha_content.txt');
		}

		//  [0] build hash
		$sHash = LEPTON_handle::createGUID();
		$_SESSION['addon_info_hash'] = $sHash;

		// data for twig template engine
		$templateValues = array(
			'oADDON_INFO'	=> $this,
			"hash"			=> $sHash,
			'content'		=> $content,
			'readme_link'	=> "https://cms-lab.com/_documentation/addon-info/readme.php",
			'leptoken'		=> get_leptoken()

		);

		//	get the template-engine
		$oTWIG = lib_twig_box::getInstance();
		$oTWIG->registerModule('addon_info');

		echo $oTWIG->render(
			"@addon_info/tool.lte",	//	template-filename
			$templateValues						//	template-data
		);

	}

	/** =========================================================================
	 *
	 * Show an info popup
	 *
	 * @access  public
	 * @param   $modvalues  As optional array containing module specialized values
	 * @param   $bPrompt    True for direct output via echo, false for returning the generated source.
	 * @return  mixed       Depending on the $bPrompt param: boolean or string.
	 */
	public function showmodinfo( $modvalues = null, $bPrompt = true )
	{
		// prepare array with module specific values
		$modvalues = array(
			"BUTTONS"		=> array(
				"README"		=> array( "AVAILABLE"	=> true
					,"URL"			=> "https://cms-lab.com/_documentation/addon-info/readme.php"
				)
			)
		);

		// show module info
		$sSource = parent::showmodinfo( $modvalues );

		return $sSource;
	}

}
